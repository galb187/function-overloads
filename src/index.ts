import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

const response = saySomething("hello");
log.magenta(response);

/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

function random():number;
function random(isFloat:boolean,max?:number):number;
function random(isFloat:boolean,min?:number, max?:number):number;

function random(isFloat?:boolean, n1?:number, n2?:number){
    //no args
    if(isFloat === undefined){
        const rand = Math.random()
        if(rand < 0.5) return 0
        else return 1
    }
    //1 arg
    else if(isFloat !== undefined && n1 === undefined){
        const rand = Math.random()
        if(isFloat){
            return rand
        }
    } 
    //2arg
    else if(isFloat !== undefined && n1 !== undefined && n2 === undefined){
        const rand = Math.random()
        //int
        if(!isFloat){
            return Math.floor(rand * n1)
        }
        //float
        else{
            console.log(n1);
            console.log(rand);
            
            return rand * n1
        }
    }
    //3 arg
    else if(isFloat !== undefined && n1 !== undefined && n2 !== undefined){
        const rand = Math.random()
        //int
        if(!isFloat){
            return Math.floor(n1 + rand * (n2 - n1))
        }
        //float
        else{
            return n1 + rand * (n2 - n1)
        }
    }
}


const num1 = random(); // 0 ~ 1 | integer
console.log('0 or 1 :', {num1});

const num3 = random(false, 6); // 0 ~ 6 | integer,
console.log('int bet 0 to 6', {num3});

const num4 = random(false, 2, 6); // 2 ~ 6 | integer
console.log('int bet 2 to 6', {num4});

const num2 = random(true); // 0 ~ 1 | float
console.log('float bet 0-1:', {num2});

const num5 = random(true, 6); // 0 ~ 6 | float
console.log('float bet 0-6:',{num5});

const num6 = random(true, 2, 6); // 2 ~ 6 | float
console.log('float bet 2-6:',{num6});

//---------------------------------------------------------------
